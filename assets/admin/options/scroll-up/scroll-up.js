jQuery(document).ready(function( $ ) {

    // Back to top button
    // source: http://html-tuts.com/back-to-top-button-jquery/
    $('body').prepend('<a href="#" class="back-to-top" title="Back to Top">Back to Top</a>');
    var amountScrolled = 700;
    $(window).scroll(function() {
      if ($(window).scrollTop() > amountScrolled) {
          $('a.back-to-top').fadeIn('slow');
      } else {
          $('a.back-to-top').fadeOut('slow');
      }
    });
    $('a.back-to-top').click(function() {
      $('html,body').animate({
          scrollTop: 0
      }, 700);
      return false;
    });

})
