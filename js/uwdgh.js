// The document ready event executes when the HTML-Document is loaded
// and the DOM is ready.
jQuery(document).ready(function( $ ) {

})

// The window load event executes after the document ready event,
// when the complete page is fully loaded.
jQuery(window).load(function () {

    // remove table-striped class and border setting forced by the uw-2014 theme
    // by finding the class table-clear-default
    $('table.table.table-striped.table-clear-default').removeClass('table-striped').attr( "border", 0 );

})
